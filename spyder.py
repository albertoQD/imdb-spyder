from bs4 import BeautifulSoup
import urllib2
import sqlite3

GOO_UA = 'Mozilla/5.0 (compatible; Googlebot/2.1; \
+http://www.google.com/bot.html)'
URL_BASE = 'http://www.imdb.com'

if __name__ == "__main__":
    # DB Init
    conn = sqlite3.connect('imdb.db')
    cursor = conn.cursor()

    # Get movie list from DB
    cursor.execute('SELECT rowid, url, title FROM movies')
    movies = cursor.fetchall()

    for movie in movies:
        print ' > Processing movie: ', movie[2]
        # Fetch the html from imdb
        request = urllib2.Request(URL_BASE + movie[1] + 'fullcredits', None,
                                  {'User-Agent': GOO_UA})
        html = urllib2.urlopen(request)

        # Process the html
        soup = BeautifulSoup(html.read())
        cast = soup.select('table[class="cast"] td[class="nm"] > a')
        for actor in cast:
            actor_name = actor.text
            actor_url = actor.get('href')
            print ' > Processing actor: ', actor_name, ' URL: ', actor_url

            # check if the actor is already in DB
            cursor.execute('SELECT rowid FROM actors WHERE url=?',
                           (actor_url,))
            actors = cursor.fetchall()
            last_id = -1
            if len(actors) == 0:
                # Insert the new actor in DB
                cursor.execute('INSERT INTO actors (name, url) VALUES(?,?)',
                               (actor_name, actor_url))
                last_id = cursor.lastrowid
            else:
                last_id = actors[0][0]

            # insert the tuple movie_id, actor_id
            cursor.execute('INSERT INTO movie_actor(movie_id, actor_id) VALUES\
(?,?)', (movie[0], last_id))

    conn.commit()
    conn.close()
