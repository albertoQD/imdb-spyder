from bs4 import BeautifulSoup
import urllib2
import sqlite3

GOO_UA = 'Mozilla/5.0 (compatible; Googlebot/2.1; \
+http://www.google.com/bot.html)'
URL = 'http://www.imdb.com/chart/top'

if __name__ == "__main__":
    # DB Init
    conn = sqlite3.connect('imdb.db')
    cursor = conn.cursor()

    # Make the request to get the HTML
    request = urllib2.Request(URL, None, {'User-Agent': GOO_UA})
    html = urllib2.urlopen(request)

    # Process the html
    soup = BeautifulSoup(html.read())
    # Select all the rows in the table that has movie info
    movies = soup.select('tr[valign="top"] > td:nth-of-type(3) a')
    for movie in movies:
        movie_url = movie.get('href')
        movie_title = movie.text
        cursor.execute('INSERT INTO movies (title, url) VALUES (?, ?)',
                       (movie_title, movie_url))
    conn.commit()
    conn.close()
